import React, { useContext } from 'react'
import Home from './pages/Home';
import Dashboard from './pages/Dashboard';
import { userContext } from './contexts/UserContext';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Settings from './pages/Settings';
import Todo from './pages/Todo';


export const AppRoutes = () => {

  let { data } = useContext(userContext);

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <> {data.status ? <>
          <Route path="/dashboard" element={<Dashboard />} />
          <Route path="/settings" element={<Settings />} />
        </> : <>user not logged in</>
        }</>
        <Route path={"/todo/:id"} element={<Todo />} />
        <Route path="/todo/" element={<Todo />} />
        <Route path="*" element={<h1>No such path avalible</h1>} />
      </Routes>
    </BrowserRouter>
  )
}
