import './App.css';
import UserContext from './contexts/UserContext';
import { AppRoutes } from './AppRoutes';
function App() {
   
  return (
     
    <div className='App'>
      <UserContext> 
       <AppRoutes />
       </UserContext>
    </div>
  );
}


export default App;

 