import React, { Component } from 'react'

const CounterHoc = (Actual) => {
class newCounter extends Component {
    constructor(props){
        super(props);
        this.state={
            count:0
        }
    }
    increment=()=>{
        this.setState( {count:this.state.count +1}
        )
    }
  render() {
    return (
    
    <Actual 
    incCount={this.increment} 
    count={this.state.count}/>
    )
    
  }
}

  return newCounter
}
export default CounterHoc;