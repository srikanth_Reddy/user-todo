import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import './Dashboard.css'
import CounterHoc from '../CounterHoc';
class Dashboard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoaded: false,
            data: [],
            error: null
        }

    }
    componentDidMount() {

        fetch('https://jsonplaceholder.typicode.com/todos')
            .then((items) => items.json())
            .then((res) => {
                this.setState({
                    data: res.slice(0, 10),
                    isLoaded: true
                })
            }, (error) => {
                this.setState({
                    isLoaded: true,
                    error
                })
            }
            )
    }
    render() {
        const { error, data, isLoaded } = this.state;

        if (error) {
            return <div>Error:{error.message}</div>
        } else if (!isLoaded) {
            return <div>Loading...</div>
        } else {

            return <div className='dashboard' >
                <button className="btn" onClick={this.props.incCount}>Clicked:{this.props.count}-times</button>
                <h2>Events</h2>
                <div>
                    <table>
                        <thead>
                            <tr>
                                <th scope='col'>Id</th>
                                <th scope='col'>Title</th>
                                <th scope='col'>Completed</th>
                            </tr>
                        </thead>
                        <tbody>{data.map((item) => (
                            <tr key={item.id}>
                                <td>{item.id}</td>
                                <td>{item.title}</td>
                                <td>{(item.completed) ? "true" : "false"}</td>
                            </tr>)
                        )}

                        </tbody>
                    </table>
                </div>
                <br />
                <div><button className="settings"> <Link to="/settings">Settings</Link></button></div>
            </div>

        }

    }
}
export default CounterHoc(Dashboard)