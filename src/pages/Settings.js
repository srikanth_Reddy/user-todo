import React, { useState } from 'react'
import { useContext } from 'react';
import CounterHoc from '../CounterHoc';
import './Settings.css'
import { userContext } from '../contexts/UserContext';


const Settings = (props) => {

    let { data, setData } = useContext(userContext);

    let [err, setErr] = useState(
        {
            err1: "", err2: ""
        })

    let changeUserName = (e) => {
        e.preventDefault();
        if (e.target.oldPass.value === data.password) {
            setData({
                ...data, username: e.target.newUser.value
            })
        } else {
            setErr({ ...err, err1: "Password Incorrect" })
        }
    }

    let changePassword = (e) => {
        if (e.target.oldPass.value === data.password) {
            setData({
                ...data, password: e.target.newPass.value
            })
        } else {
            setErr({ ...err, err2: "Password Incorrect" })
        }
    }

    return (
        <div className='settings'>

            <h1>Settings</h1>

            <button className="btn" onClick={props.incCount}>Clicked:{props.count}-times</button>

            <form className="settings1" onSubmit={changeUserName}>
                <h2>Change username</h2>
                <div>
                    <label>Enter new username:</label>
                    <input type="text" name="newUser" ></input>
                </div>
                <div>
                    <label>Enter Password:</label>
                    <input type="text" name="oldPass"></input>
                </div>
                <button type='submit'>Submit</button>
                <div className='errMsg'>{err.err1}</div>
            </form >


            <form className="settings2" onSubmit={changePassword}>
                <h2>Change password</h2>
                <div>
                    <label>Enter new password:</label>
                    <input type="text" name="newPass"></input>
                </div>
                <div>
                    <label>Enter old password:</label>
                    <input type="text" name="oldPass"></input>
                </div>
                <button type='submit'>Submit</button>
                <div className='errMsg'>{err.err2}</div>
            </form>
        </div>
    )
}
export default CounterHoc(Settings);


