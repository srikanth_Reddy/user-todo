import React, { useEffect } from 'react'
import { useParams, useSearchParams } from 'react-router-dom'
import { useState } from 'react';
import CounterHoc from '../CounterHoc';
function Todo(props) {
    let [params] = useSearchParams();
    
    let { id } = useParams();
    let ids = params.getAll("id")
    if (id) {
        ids.push(id)
    }
    let [data, setData] = useState({
        userTodo: [], isLoaded: false
    })
    let QueryStr = ids.reduce((acc, cur) => {
        if (acc.length === 0) {
            acc += ("id=" + cur);
        }
        else {
            acc += ("&id=" + cur)
        }
        return acc;
    }, "");
    useEffect(()=>{
        fetch(`https://jsonplaceholder.typicode.com/todos?${QueryStr}`)
        .then((res) => res.json())
        .then((result) => {
            setData({
                userTodo: result, isLoaded: true
            })
        },
            (err) => {
                setData({
                    isLoaded: true, err
                })

            })
    },[QueryStr])
    if (data.err) {
        return <div>Error:{data.err}</div>
    }
    else if (!data.isLoaded) {
        return <div >Loading...</div>

    }
    else if (data.userTodo.length < 1) {
        return <div>No User found with that id</div>
    }
    else {
        return <div className='dashboard'>
             <button className='btn' onClick={props.incCount}>Clicked:{props.count}-times</button>
            <h2>Events</h2>
            <table>
                <thead>
                    <tr>
                        <th scope='col'>Id</th>
                        <th scope='col'>Title</th>
                        <th scope='col'>Completed</th>
                    </tr>
                </thead>
                <tbody>{data.userTodo.map((item) => (
                    <tr key={item.id}>
                        <td>{item.id}</td>
                        <td>{item.title}</td>
                        <td>{(item.completed) ? "true" : "false"}</td>
                    </tr>)
                )}
                </tbody>
            </table><br />
        </div>
    }
}
export default CounterHoc(Todo)