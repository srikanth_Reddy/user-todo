import React, { useContext } from 'react'
import './Home.css'
import { useNavigate } from "react-router-dom"
import CounterHoc from '../CounterHoc';
import { userContext } from '../contexts/UserContext';

function Home(props) {

    const navigate = useNavigate();

    const { data, setData } = useContext(userContext)

    let handleSubmit = (event) => {
        event.preventDefault();

        if (event.target.username.value === data.username && event.target.password.value === data.password) {
            navigate('/dashboard')
            setData({ ...data, status: true })
        } else {
            setData({ ...data, error: "username/password wrong" })
        }
    }

    let onfocus = () => {
        setData({ ...data, error: "" })
    }

    return (<div className='home'>
        <button className='btn' onClick={props.incCount}>Clicked:{props.count}-times</button>
        <form className="login form" onSubmit={handleSubmit} >
            <h1>TODO APP</h1>
            <div>
                Username:
                <input name="username" type="text" onFocus={onfocus} placeholder="Enter username..."></input>
            </div><br />
            <div>
                Password:
                <input name="password" type="text" onFocus={onfocus} placeholder="Enter Password..."></input>
            </div><br />
            <div> <button type="submit">LOG IN</button></div>
            <div className='errMsg'>{data.error}</div>
        </form>
    </div>
    )
}
export default CounterHoc(Home);


