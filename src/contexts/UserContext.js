import React, { createContext } from 'react'
import { useState } from 'react';
export const userContext = createContext();

function UserContext(props) {
  let [data, setData] = useState({
    username: "srikanth",
    password: "srikanth123",
    error: "",
    status: false
  })

  return <userContext.Provider value={{ data, setData }}>
    {props.children}
  </userContext.Provider>

}
export default UserContext
